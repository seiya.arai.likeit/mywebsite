<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品マスタ一覧</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
</head>

<!-- body -->
<body>
	<form method="post" action="ItemMasterListServlet"
		class="form-horizontal">
		<div class="container">
			<div class="card mx-auto" style="width: 70rem;">
				<div class="card-body">
					<!-- header -->
					<header>
						<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
							<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
								<li class="nav-item active"><a class="nav-link"
									href="ItemMasterListServlet">himawari管理システム</a></li>
							</ul>
							<ul class="navbar-nav flex-row">
								<li class="nav-item"><a class="nav-link" href="#">${loginManager.name}</a>
								</li>
								<li class="nav-item"><a class="btn btn-primary"
									href="LogoutMasterServlet">ログアウト</a></li>
							</ul>
						</nav>
					</header>
					<!-- /header -->

					<!-- 新規登録ボタン -->
					<div class="text-right">
						<a href="ItemMasterCreateServlet">キャンプギア新規登録</a>
					</div>


					<!-- 検索ボックス -->
					<div class="search-form-area">
						<div class="panel-body">
							<div class="form-group row">
								<label for="loginId" class="col-sm-3 col-form-label">商品ID</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="itemId" name="id">
								</div>
							</div>

							<div class="form-group row">
								<label for="userName" class="col-sm-3 col-form-label">商品名</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="itemName"
										name="name">
								</div>
							</div>

							<div class="form-group row">
								<label for="userName" class="col-sm-3 col-form-label">ブランド</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="burandName"
										name="burandName">
								</div>
							</div>

							<div class="form-group row">

								<label for="birthDate" class="col-sm-3 col-form-label">登録日</label>

								<div class="row col-sm-8">
									<div class="col-sm-5">
										<input type="date" name="startDate" id="date-start"
											class="form-control" />
									</div>

									<div class="col-sm-1 text-center">~</div>
									<div class="col-sm-5">
										<input type="date" name="endDate" id="date-end"
											class="form-control" />
									</div>
								</div>
							</div>
							<div class="text-right">
								<button type="submit" value="検索"
									class="btn btn-primary form-submit">検索</button>
							</div>
						</div>
					</div>

					<!-- 検索結果一覧 -->
					<div class="table-responsive">
						<table class="table table-striped">
							<thead class="thead-dark">
								<tr>
									<th>商品ID</th>
									<th>商品名</th>
									<th>ブランド</th>
									<th>登録日</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="itemList" items="${itemMasterList}">
									<tr>
										<td>${itemList.id}</td>
										<td>${itemList.name}</td>
										<td>${itemList.burandName}</td>
										<td>${itemList.createDate}</td>
										<td><a class="btn btn-primary"
											href="ItemMasterDetailServlet?id=${itemList.id }">詳細</a> <a
											class="btn btn-success"
											href="ItemMasterUpdateServlet?id=${itemList.id }">更新</a> <a
											class="btn btn-danger"
											href="ItemMasterDeleteServlet?id=${itemList.id }">削除</a></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div class="text-right">
					<a href="UserMasterListServlet">ユーザー管理へ</a>
				</div>

			</div>
		</div>
	</form>
</body>
</html>
