<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/original/common.css" rel="stylesheet">

</head>

<body>
	<div class="container">
		<div class="card mx-auto" style="width: 60rem;">
			<div class="card-body">

				<header>
					<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
						<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
							<li class="nav-item active"><a class="nav-link"
								href="ItemMasterListServlet">himawari管理システム</a></li>
						</ul>
						<ul class="navbar-nav flex-row">
							<li class="nav-item"><a class="nav-link" href="#">${loginManager.name}</a>
							</li>
							<li class="nav-item"><a class="btn btn-primary"
								href="LogoutMasterServlet">ログアウト</a></li>
						</ul>
					</nav>
				</header>

				<div class="text-center">
					<h1>ユーザ情報更新</h1>
				</div>
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>

				<form action="UserMasterUpdateServlet" method="post">
				<input type="hidden" class="form-control" id="inputPassword" name="id" value="${userMasterDetail.id}">
					<div class="form-group row">
						<label for="staticLoginId" class="col-sm-3 col-form-label">ログインID</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputloginId"
								name="loginId" value="${userMasterDetail.loginId}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputPassword" class="col-sm-3 col-form-label">パスワード</label>
						<div class="col-sm-8">
							<input type="password" class="form-control" id="inputPassword"
								name="password">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputPassword" class="col-sm-3 col-form-label">パスワード（確認）</label>
						<div class="col-sm-8">
							<input type="password" class="form-control" id="inputPassword"
								name="password2">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputUserName" class="col-sm-3 col-form-label">ユーザ名</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputUserName"
								name="name" value="${userMasterDetail.name}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputDate" class="col-sm-3 col-form-label">生年月日</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputDate"
								name="birthDate" value="${userMasterDetail.birthDate}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputUserName" class="col-sm-3 col-form-label">郵便番号</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputUserName"
								name="postalCode" value="${userMasterDetail.postalCode}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputUserName" class="col-sm-3 col-form-label">住所</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputUserName"
								name="address" value="${userMasterDetail.address}">
						</div>
					</div>

					<div class="form-group row">
						<label for="inputUserName" class="col-sm-3 col-form-label">電話番号</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="inputUserName"
								name="phoneNumber" value="${userMasterDetail.phoneNumber}">
						</div>
					</div>

					<div class="text-center">
						<button type="submit" value="検索"
							class="btn btn-primary center-block form-submit">登録</button>
					</div>

					<div class="text-right">
						<a href="UserMasterListServlet">戻る</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>