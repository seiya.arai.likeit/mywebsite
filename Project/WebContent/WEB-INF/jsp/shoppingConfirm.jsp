<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css"
	rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />

</head>
<body>

	<!DOCTYPE html>
	<nav class="yellow darken-3" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="HomeServlet" class="brand-logo">himawari</a>
			<ul class="right">



				<li><a href="HomeServlet"><i class="material-icons">home</i></a></li>


				<li><a href="ShoppingCartServlet"><i class="material-icons">shopping_cart</i></a></li>


				<li><a href="LogoutServlet"><i class="material-icons">exit_to_app</i></a></li>

			</ul>
		</div>
	</nav>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h4 class=" col s12 light">
				カートアイテム<i class="material-icons center">shopping_cart</i>
			</h4>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="ShoppingBuyServlet" method="POST">
							<div class="row">
								<table class="bordered">
									<thead>
										<tr>
											<th class="center" style="width: 20%">商品名</th>
											<th class="center">単価</th>
											<th class="center" style="width: 20%">小計</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="cartInItem" items="${cart}">
											<tr>
												<td class="center">${cartInItem.name}</td>
												<td class="center">${cartInItem.formatPrice}円(税別)</td>
												<td class="center">${cartInItem.includePrice}円(税込)</td>
											</tr>
										</c:forEach>
										<tr>
											<td class="center"></td>
											<td class="center"></td>
											<td class="center"><label>配送方法</label> <select
												class="browser-default" name="delivery_method_id">
													<option value="" disabled selected>配送方法を選択してください。</option>
													<c:forEach var="dmdb" items="${dmdbList}">
														<option value="${dmdb.id}">${dmdb.name}</option>
													</c:forEach>
											</select></td>
										</tr>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col s12">
									<button
										class="btn brown  waves-effect waves-light  col s4 offset-s4"
										type="submit" name="action">購入する</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row center">
			<nav class=" darken-3 " role="navigation">
				<div class="nav-wrapper container">
					<h6 class=" col s12 light black-text">最近チェックしたキャンプギア</h6>
				</div>
			</nav>
		</div>
		<div class="section">
			<!--   おすすめ商品   -->
			<div class="row">
				<c:forEach var="detail" items="${browsing}">
					<div class="col s12 m3">
						<div class="card">
							<div class="card-image">
								<a href="ItemDetailServlet?item_id=${detail.id}"><img
									src="img/${detail.fileName}"></a>
							</div>
							<div class="card-content">
								<span class="card-title">${detail.name}</span>
								<p>ブランド：${detail.burandName}</p>
								<p>価格：${detail.formatPrice}円(税別)</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>



			<!DOCTYPE html>
			<footer class="page-footer yellow darken-3">
				<div class="container">
					<div class="right-align">Made by himawari</div>
				</div>
			</footer>
</body>
</html>