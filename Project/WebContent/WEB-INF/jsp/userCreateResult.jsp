<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録完了画面</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css"
	rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<!-- 共通独自CSS -->

</head>
<body>

	<!DOCTYPE html>
	<nav class="yellow darken-3" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="Index" class="brand-logo">himawari</a>
			<ul class="right">



				<li><a href="UserDataServlet"><i class="material-icons">person_add</i></a></li>


				<li><a href="ShoppingCartServlet"><i class="material-icons">local_grocery_store</i></a></li>


				<li><a href="LoginServlet"><i class="material-icons">touch_app</i></a></li>

			</ul>
		</div>
	</nav>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">登録完了</h5>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input type="text" value="${udb.name}" readonly> <label>名前</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${birthDate}" type="date" required> <label>生年月日</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${udb.postalCode}" type="text" required> <label>郵便番号</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input type="text" value="${udb.address}" readonly> <label>住所</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input value="${udb.phoneNumber}" type="text" required>
								<label>電話番号</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s10 offset-s1">
								<input type="text" value="${udb.loginId}" readonly> <label>ログインID</label>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">上記内容で登録しました。</p>
							</div>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<a href="LoginServlet"
										class="btn brown waves-effect waves-light  col s8 offset-s2">ログイン画面へ</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!DOCTYPE html>
	<footer class="page-footer yellow darken-3">
		<div class="container">
			<div class="right-align">Made by himawari</div>
		</div>
	</footer>
</body>
</html>