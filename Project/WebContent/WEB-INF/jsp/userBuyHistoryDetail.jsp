<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入履歴詳細</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css"
	rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<!-- 共通独自CSS -->
<style type="text/css">
.card-content>span {
	font-size: 18px !important;
	height: 150px;
}
</style>
</head>
<body>

	<!DOCTYPE html>
	<nav class="yellow darken-3" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="HomeServlet" class="brand-logo">himawari</a>
			<ul class="right">



				<li><a href="UserDataServlet"><i class="material-icons">account_circle</i></a></li>


				<li><a href="ShoppingCartServlet"><i class="material-icons">shopping_cart</i></a></li>


				<li><a href="LogoutServlet"><i class="material-icons">exit_to_app</i></a></li>

			</ul>
		</div>
	</nav>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h4 class=" col s12 light">購入詳細</h4>
		</div>
		<!--  購入 -->
		<div class="row">
			<div class="col s10 offset-s1">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table class="bordered">
							<thead>
								<tr>
									<th class="center">商品名</th>
									<th class="center">ブランド</th>
									<th class="center" style="width: 20%">単価</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="bdd3" items="${bdd3}">
									<tr>
										<td class="center">${bdd3.name}</td>
										<td class="center">${bdd3.burandName}</td>
										<td class="center">${bdd3.formatPrice}円(税別)</td>
									</tr>
								</c:forEach>

								<tr>
									<td class="center">${bdd2.deliveryMethodName}</td>
									<td class="center"></td>
									<td class="center">${bdd2.deliveryMethodPrice}円(税別)</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- 詳細 -->
		<div class="row">
			<div class="col s10 offset-s1">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table class="bordered">
							<table>
								<thead>
									<tr>
										<th class="center" style="width: 20%;">購入日時</th>
										<th class="center">配送方法</th>
										<th class="center" style="width: 20%">合計金額</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="center">${bdd2.formatDate}</td>
										<td class="center">${bdd2.deliveryMethodName}</td>
										<td class="center">${bdd2.formatTotalPrice}円(税込)</td>
									</tr>
								</tbody>
							</table>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s8 offset-s2 ">
			<p class="right-align">
				<a href="UserDataServlet">戻る</a>
			</p>
		</div>
	</div>

	<footer class="page-footer yellow darken-3">
		<div class="container">
			<div class="right-align">Made by himawari</div>
		</div>
	</footer>

</body>
</html>