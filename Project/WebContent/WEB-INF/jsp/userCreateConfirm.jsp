<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規登録入力内容確認画面</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />
<!-- 共通独自CSS -->

</head>
<body>

<!DOCTYPE html>
<nav class="yellow darken-3" role="navigation">
	<div class="nav-wrapper container">
		<a id="logo-container" href="Index" class="brand-logo">himawari</a>
		<ul class="right">



			<li><a href="UserDataServlet"><i class="material-icons">person_add</i></a></li>


			<li><a href="ShoppingCartServlet"><i class="material-icons">local_grocery_store</i></a></li>


			<li><a href="LoginServlet"><i class="material-icons">touch_app</i></a></li>

		</ul>
	</div>
</nav>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h4 class=" col s12 light">入力内容確認</h4>
		</div>
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="UserCreateResultServlet" method="POST">
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input type="text" name="name" value="${udb.name}" readonly> <label>名前</label>
								</div>
							</div>
                            <div class="row">
								<div class="input-field col s10 offset-s1">
									<input value="${birthDate}" name="birthDate" type="date" required> <label>生年月日</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input value="${udb.postalCode}" name="postalCode" type="text" required> <label>郵便番号</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input type="text" name="address" value="${udb.address}" readonly> <label>住所</label>
								</div>
							</div>
                            <div class="row">
								<div class="input-field col s10 offset-s1">
									<input value="${udb.phoneNumber}" name="phoneNumber" type="text" required> <label>電話番号</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input type="text" name="loginId" value="${udb.loginId}" readonly> <label>ログインID</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s10 offset-s1">
									<input type="password" name="password" value="${udb.password}" readonly> <label>パスワード</label>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<p class="center-align">上記内容で登録してよろしいでしょうか?</p>
								</div>
							</div>
							<div class="row">
								<div class="col s6 center-align">
									<button class="btn brown  waves-effect waves-light" type="submit" name="confirm_button" value="cancel">修正</button>
								</div>
								<div class="col s6 center-align">
									<button class="btn brown  waves-effect waves-light" type="submit" name="confirm_button" value="regist">登録</button>
								</div>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<!DOCTYPE html>
<footer class="page-footer yellow darken-3">
		<div class="container">
        <div class="right-align">
			Made by himawali
            </div>
		</div>
	</footer>