<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css"
	rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />

</head>
<body>

	<!DOCTYPE html>
	<nav class="yellow darken-3" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="HomeServlet" class="brand-logo">himawari</a>
			<ul class="right">



				<li><a href="UserDataServlet"><i class="material-icons">account_circle</i></a></li>


				<li><a href="ShoppingCartServlet"><i class="material-icons">shopping_cart</i></a></li>


				<li><a href="LoginServlet"><i class="material-icons">vpn_key</i></a></li>

			</ul>
		</div>
	</nav>
	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<br> <br>
			<div class="row center">
				<div class="col s4">
					<c:if test="${searchWord != null}">
						<a
							href="ItemSearchResultServlet?search_word=${searchWord}&page_num=${pageNum}"
							class="btn waves-effect waves-light">検索結果へ </a>
					</c:if>
				</div>
				<div class="col s4">
					<h4 class=" col s12 light">商品詳細</h4>
				</div>
			</div>
			<br> <br>
			<div class="row">
				<div class="col s6">
					<div class="card">
						<div class="card-image">
							<img src="img/${item.fileName}">
						</div>
					</div>
				</div>
				<div class="col s6">
					<h4>${item.name}</h4>
					<p>${item.burandName}</p>
					<p>${item.detail}</p>
					<p>${item.spec}</p>
					<h5>${item.formatPrice}円（税別）</h5>
					<div class="right-align">
						<form action="ShoppingCartServlet" method="POST">
							<input type="hidden" name="item_id" value="${item.id}">
							<button class="btn  brown waves-effect waves-light" type="submit"
								name="action">
								買い物かごに追加 <i class="material-icons right">add_shopping_cart</i>
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>


		<!DOCTYPE html>
		<footer class="page-footer yellow darken-3">
			<div class="container">
				<div class="right-align">Made by himawari</div>
			</div>
		</footer>
	</div>
</body>
</html>