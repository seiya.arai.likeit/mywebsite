<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品マスタ情報詳細</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">

</head>

<body>
	<div class="container">
		<div class="card mx-auto" style="width: 60rem;">
			<div class="card-body">

				<!-- header -->
				<header>
					<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
						<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
							<li class="nav-item active"><a class="nav-link"
								href="ItemMasterListServlet">himawari管理システム</a></li>
						</ul>
						<ul class="navbar-nav flex-row">
							<li class="nav-item"><a class="nav-link" href="#">${loginManager.name}</a>
							</li>
							<li class="nav-item"><a class="btn btn-primary"
								href="LogoutMasterServlet">ログアウト</a></li>
						</ul>
					</nav>
				</header>
				<!-- /header -->

				<!-- body -->
				<div class="container">




					<div class="form-group row">
						<label for="loginId" class="col-sm-2 col-form-label">商品ID</label>
						<div class="col-sm-10">
							<td>${item.id}</td>
						</div>
					</div>

					<div class="form-group row">
						<label for="userName" class="col-sm-2 col-form-label">商品名</label>
						<div class="col-sm-10">
							<td>${item.name}</td>
						</div>
					</div>

					<div class="form-group row">
						<label for="birthDate" class="col-sm-2 col-form-label">ブランド</label>
						<div class="col-sm-10">
							<td>${item.burandName}</td>
						</div>
					</div>

					<div class="form-group row">
						<label for="birthDate" class="col-sm-2 col-form-label">商品詳細</label>
						<div class="col-sm-10">
							<td>${item.detail}</td>
						</div>
					</div>

					<div class="form-group row">
						<label for="birthDate" class="col-sm-2 col-form-label">スペック</label>
						<div class="col-sm-10">
							<td>${item.spec}</td>
						</div>
					</div>

					<div class="form-group row">
						<label for="birthDate" class="col-sm-2 col-form-label">価格</label>
						<div class="col-sm-10">
							<td>${item.formatPrice}円(税別)</td>
						</div>
					</div>

					<div class="form-group row">
						<label for="createDate" class="col-sm-2 col-form-label">新規登録日時</label>
						<div class="col-sm-10">
							<td>${item.createDate}</td>
						</div>
					</div>

					<div class="form-group row">
						<label for="updateDate" class="col-sm-2 col-form-label">更新日時</label>
						<div class="col-sm-10">
							<td>${item.updateDate}</td>
						</div>
					</div>

					<div class="form-group row">
						<label for="updateDate" class="col-sm-2 col-form-label">写真</label>
						<div class="col-sm-10">
							<img src="img/${item.fileName}" width="300" height="300">
						</div>
					</div>

				</div>
			</div>
			<div class="text-right">
				<a href="ItemMasterListServlet">戻る</a>
			</div>
		</div>
	</div>
</body>

</html>
