<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>キャンプギアマスタ更新画面</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">

</head>

<body>
	<div class="container">
		<div class="card mx-auto" style="width: 60rem;">
			<div class="card-body">
				<!-- header -->
				<header>
					<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
						<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
							<li class="nav-item active"><a class="nav-link"
								href="ItemMasterListServlet">himawari管理システム</a></li>
						</ul>
						<ul class="navbar-nav flex-row">
							<li class="nav-item"><a class="nav-link" href="#">${loginManager.name}</a>
							</li>
							<li class="nav-item"><a class="btn btn-primary"
								href="LogoutMasterServlet">ログアウト</a></li>
						</ul>
					</nav>
				</header>
				<!-- /header -->

				<!-- body -->
				<div class="container">

					<form method="post" action="ItemMasterUpdateServlet"
						class="form-horizontal">
						<div class="form-group row">
							<label for="loginId" class="col-sm-2 col-form-label">商品ID</label>
							<div class="col-sm-10">
								<input type="hidden" class="form-control" id="inputPassword"
									name="id" value="${item.id}">
							</div>
						</div>

						<div class="form-group row">
							<label for="password" class="col-sm-2 col-form-label">商品名</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="itemName"
									name="name" value="${item.name}">
							</div>
						</div>

						<div class="form-group row">
							<label for="passwordConf" class="col-sm-2 col-form-label">ブランド</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="itemBrand"
									name="burandName" value="${item.burandName}">
							</div>
						</div>

						<div class="form-group row">
							<label for="userName" class="col-sm-2 col-form-label">価格</label>
							<div class="col-sm-10">
								<input type="value" class="form-control" id="userName"
									name="price" value="${item.price}">
							</div>
						</div>

						<div class="form-group row">
							<label for="birthDate" class="col-sm-2 col-form-label">商品詳細</label>
							<div class="col-sm-10">
								<textarea class="form-control" id="exampleFormControlTextarea1"
									rows="4" name="detail">${item.detail}</textarea>
							</div>
						</div>

						<div class="form-group row">
							<label for="birthDate" class="col-sm-2 col-form-label">スペック</label>
							<div class="col-sm-10">
								<textarea class="form-control" id="exampleFormControlTextarea1"
									rows="4" name="spec">${item.spec}</textarea>
							</div>
						</div>

						<div class="submit-button-area">
							<button type="submit" value="検索"
								class="btn btn-primary btn-lg btn-block">登録</button>
						</div>

						<div class="text-right">
							<a href="ItemMasterListServlet">戻る</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
