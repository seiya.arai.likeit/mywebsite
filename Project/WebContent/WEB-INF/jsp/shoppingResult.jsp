<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入完了</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet" media="screen,projection" />

</head>
<body>

<!DOCTYPE html>
<nav class="yellow darken-3" role="navigation">
	<div class="nav-wrapper container">
		<a id="logo-container" href="HomeServlet" class="brand-logo">himawari</a>
		<ul class="right">



			<li><a href="HomeServlet"><i class="material-icons">home</i></a></li>


			<li><a href="ShoppingCartServlet"><i class="material-icons">shopping_cart</i></a></li>


			<li><a href="LogoutServlet"><i class="material-icons">exit_to_app</i></a></li>

		</ul>
	</div>
</nav>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h4 class=" col s12 ">購入が完了しました</h4>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="col s6 center-align">
					<a href="HomeServlet" class="btn brown  waves-effect waves-light ">買い物を続ける</a>
				</div>
				<div class="col s6 center-align">
					<a href="UserDataServlet" class="btn brown  waves-effect waves-light">会員情報へ</a>
				</div>
			</div>
		</div>
		<div class="row center">
			<h5 class=" col s12 light">購入詳細</h5>
		</div>
		<!--  購入 -->
		<div class="row">
			<div class="section"></div>
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table>
							<thead>
								<tr>
									<th class="center">購入日時</th>
									<th class="center">配送方法</th>
									<th class="center">合計金額</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="center">${resultBDB.formatDate}</td>
									<td class="center">${resultBDB.deliveryMethodName}</td>
									<td class="center">${resultBDB.formatTotalPrice}円(税込)</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- 詳細 -->
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table class="bordered">
							<thead>
								<tr>
									<th class="center">商品名</th>
									<th class="center">単価</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="buyIDB" items="${buyIDBList}" >
									<tr>
										<td class="center">${buyIDB.name}</td>
										<td class="center">${buyIDB.formatPrice}円(税別)</td>
									</tr>
								</c:forEach>
								<tr>
									<td class="center">${resultBDB.deliveryMethodName}</td>
									<td class="center">${resultBDB.deliveryMethodPrice}円(税別)</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

     <div class="container">
		<div class="row center">
            <nav class=" darken-3 " role="navigation">
	<div class="nav-wrapper container">
			<h6 class=" col s12 light black-text">最近チェックしたキャンプギア</h6>
		</div>
            </nav>
        </div>
		<div class="section">
			<!--   おすすめ商品   -->
			<div class="row">

				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="Item?item_id=98"><img src="img/joyfulmart_ys-a09553.png"></a>
						</div>
						<div class="card-content">
                            <span class="card-title">テント</span>
							<p>ブランド：ノースフェイス</p>
                            <p>価格：6,880円</p>
						</div>
					</div>
				</div>

				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="Item?item_id=70"><img src="img/low-ya_f101-g1014-100.png"></a>
						</div>
						<div class="card-content">
							<span class="card-title">テント</span>
							<p>ブランド：ノースフェイス</p>
                            <p>価格：6,880円</p>
						</div>
					</div>
				</div>

				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="Item?item_id=268"><img src="img/orizin_kmmks45240p4.png"></a>
						</div>
						<div class="card-content">
							<span class="card-title">テント</span>
							<p>ブランド：ノースフェイス</p>
                            <p>価格：6,880円</p>
						</div>
					</div>
				</div>

				<div class="col s12 m3">
					<div class="card">
						<div class="card-image">
							<a href="Item?item_id=102"><img src="img/bauhaus1_sy10-006new-wh.png"></a>
						</div>
						<div class="card-content">
							<span class="card-title">テント</span>
							<p>ブランド：ノースフェイス</p>
                            <p>価格：6,880円</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

<!DOCTYPE html>
<footer class="page-footer yellow darken-3">
		<div class="container">
            <div class="right-align">
			Made by himawari
            </div>
		</div>
	</footer>


</body>
</html>