<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>TOPページ</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css"
	rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />

</head>
<body>

	<!DOCTYPE html>
	<nav class="yellow darken-3" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="HomeServlet" class="brand-logo">himawari</a>
			<ul class="right">



				<li><a href="UserDataServlet"><i class="material-icons">account_circle</i></a></li>


				<li><a href="ShoppingCartServlet"><i class="material-icons">shopping_cart</i></a></li>


				<li><a href="LogoutServlet"><i class="material-icons">exit_to_app</i></a></li>

			</ul>
		</div>
	</nav>
	<div class="section no-pad-bot" id="index-banner">
		<div class="container">
			<br> <br>
			<h1 class="header center green-text">キャンプギアサイト</h1>
			<div class="row center">
				<h5 class="header col s12 light">キャンプのある生活。</h5>
			</div>
			<div class="row center">
				<div class="input-field col s8 offset-s2">
					<form action="ItemSearchResultServlet">
						<i class="material-icons prefix">search</i> <input type="text"
							name="search_word">
					</form>
				</div>
			</div>
			<br> <br>
		</div>
	</div>
	<div class="container">
		<div class="row center">
			<h5 class=" col s12 light">おすすめキャンプギア</h5>
		</div>
		<div class="section">
			<!--   おすすめキャンプギア   -->
			<div class="row">
				<c:forEach var="item" items="${itemList}">
					<div class="col s12 m3">
						<div class="card">
							<div class="card-image">
								<a href="ItemDetailServlet?item_id=${item.id}"><img
									src="img/${item.fileName}"></a>
							</div>
							<div class="card-content">
								<span class="card-title">${item.name}</span>
								<p>ブランド：${item.burandName}</p>
								<p>価格：${item.formatPrice}円(税別)</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row center">
			<nav class=" darken-3 " role="navigation">
				<div class="nav-wrapper container">
					<h6 class=" col s12 light black-text">最近チェックしたキャンプギア</h6>
				</div>
			</nav>
		</div>
		<div class="section">
			<!--   おすすめ商品   -->
			<div class="row">
				<c:forEach var="detail" items="${browsing}">
					<div class="col s12 m3">
						<div class="card">
							<div class="card-image">
								<a href="ItemDetailServlet?item_id=${detail.id}"><img
									src="img/${detail.fileName}"></a>
							</div>
							<div class="card-content">
								<span class="card-title">${detail.name}</span>
								<p>ブランド：${detail.burandName}</p>
								<p>価格：${detail.formatPrice}円(税別)</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</div>

	<!DOCTYPE html>
	<footer class="page-footer yellow darken-3">
		<div class="container">
			<div class="right-align">Made by himawari</div>
		</div>
	</footer>


</body>
</html>