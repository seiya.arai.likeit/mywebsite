<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート確認</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css"
	rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />

</head>
<body>

	<!DOCTYPE html>
	<nav class="yellow darken-3" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="HomeServlet" class="brand-logo">himawari</a>
			<ul class="right">



				<li><a href="HomeServlet"><i class="material-icons">home</i></a></li>


				<li><a href="ShoppingCartServlet"><i class="material-icons">shopping_cart</i></a></li>


				<li><a href="LogoutServlet"><i class="material-icons">exit_to_app</i></a></li>

			</ul>
		</div>
	</nav>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			${cartActionMessage}
			<h4 class=" col s12 light">
				買い物かご<i class="material-icons center">shopping_cart</i>
			</h4>
		</div>
		<div class="section">
			<form action="ItemDeleteServlet" method="POST">
				<div class="row">
					<c:forEach var="item" items="${cart}" varStatus="status">
						<div class="col s12 m3">
							<div class="card">
								<div class="card-image">
									<a href="ItemDetailServlet?item_id=${item.id}"><img
										src="img/${item.fileName}"> </a>
								</div>
								<div class="card-content">
									<span class="card-title">${item.name}</span>
									<p>ブランド：${item.burandName}</p>
									<p>価格：${item.formatPrice}円(税別)</p>
									<p>
										<label> <input type="checkbox" id="${status.index}"
											name="delete_item_id_list" value="${item.id}" /> <span
											for="${status.index}">削除</span>
										</label>
									</p>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="col s6 center-align">
							<button
								class="btn brown waves-effect waves-light col s6 offset-s3 "
								type="submit" name="action">
								削除<i class="material-icons right">delete</i>
							</button>
						</div>
						<div class="col s6 center-align">
							<a href="ShoppingConfirmServlet"
								class="btn brown  waves-effect waves-light col s6 offset-s3">レジに進む<i
								class="material-icons right">attach_money</i></a>
						</div>
					</div>
					<c:if test="${(status.index+1) % 4 == 0 }">
				</div>
				<div class="row">
					</c:if>
				</div>
			</form>
		</div>
	</div>

	<div class="container">
		<div class="row center">
			<nav class=" darken-3 " role="navigation">
				<div class="nav-wrapper container">
					<h6 class=" col s12 light black-text">最近チェックしたキャンプギア</h6>
				</div>
			</nav>
		</div>
		<div class="section">
			<!--   商品閲覧履歴  -->
			<div class="row">
				<c:forEach var="detail" items="${browsing}">
					<div class="col s12 m3">
						<div class="card">
							<div class="card-image">
								<a href="ItemDetailServlet?item_id=${detail.id}"><img
									src="img/${detail.fileName}"></a>
							</div>
							<div class="card-content">
								<span class="card-title">${detail.name}</span>
								<p>ブランド：${detail.burandName}</p>
								<p>価格：${detail.formatPrice}円(税別)</p>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>


			<div class="row">
				<div class="col s8 offset-s2 ">
					<p class="right-align">
						<a href="HomeServlet">戻る</a>
					</p>
				</div>
			</div>

			<!DOCTYPE html>
			<footer class="page-footer yellow darken-3">
				<div class="container">
					<div class="right-align">Made by himawari</div>
				</div>
			</footer>
</body>
</html>