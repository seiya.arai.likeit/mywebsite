<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品マスタ新規登録画面</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">

</head>

<body>
<form method="post" enctype="multipart/form-data" action="ItemMasterCreateServlet"
						class="form-horizontal">
	<div class="container">
		<div class="card mx-auto" style="width: 60rem;">
			<div class="card-body">

				<!-- header -->
				<header>
					<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
						<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
							<li class="nav-item active"><a class="nav-link"
								href="ItemMasterListServlet">himawari管理システム</a></li>
						</ul>
						<ul class="navbar-nav flex-row">
							<li class="nav-item"><a class="nav-link" href="#">${loginManager.name}</a>
							</li>
							<li class="nav-item"><a class="btn btn-primary"
								href="LogoutMasterServlet">ログアウト</a></li>
						</ul>
					</nav>
				</header>
				<!-- /header -->

				<!-- body -->
				<div class="container">

					<h1>キャンプギア新規登録</h1>
						<div class="form-group row">
							<label for="loginId" class="col-sm-2 col-form-label">商品名</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputName"
									name="name">
							</div>
						</div>

						<div class="form-group row">
							<label for="password" class="col-sm-2 col-form-label">ブランド</label>
							<div class="col-sm-10">
								<select name="burand">
								<c:forEach var="b" items="${bList}">
								<option value="${b.id}">${b.brandName}</option>
								</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group row">
							<label for="text" class="col-sm-2 col-form-label">価格</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="inputPrice"
									name="price">
							</div>
						</div>

						<div class="form-group row">
							<label for="exampleFormControlTextarea1"
								class="col-sm-2 col-form-label">商品詳細</label>
							<div class="col-sm-10">
								<textarea class="form-control" id="inputDetail" rows="4"
									name="detail"></textarea>
							</div>
						</div>

						<div class="form-group row">
							<label for="exampleFormControlTextarea1"
								class="col-sm-2 col-form-label">スペック</label>
							<div class="col-sm-10">
								<textarea class="form-control" id="inputSpec" rows="4"
									name="spec"></textarea>
							</div>
						</div>

						<div class="form-group row">
							<label for="passwordConf" class="col-sm-2 col-form-label">画像ファイル</label>
							<div class="col-sm-10">
								<input type="file" class="form-control" id="inputPrice"
									name="fileName">
							</div>
						</div>

						<div class="submit-button-area">
							<button type="submit" value="登録"
								class="btn btn-primary btn-lg btn-block">登録</button>
						</div>

						<div class="text-right">
							<a href="ItemMasterListServlet">戻る</a>
						</div>

					</form>


				</div>
			</div>
		</div>
	</div>

</body>

</html>
