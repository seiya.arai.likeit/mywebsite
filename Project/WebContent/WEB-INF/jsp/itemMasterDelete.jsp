<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>キャンプギアマスタ削除確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/original/common.css" rel="stylesheet">

</head>

<body>
	<div class="container">
		<div class="card mx-auto" style="width: 60rem;">
			<div class="card-body">


				<header>
					<nav class="navbar navbar-dark bg-dark navbar-expand  flex-md-row">
						<ul class="navbar-nav navbar-dark bg-dark flex-row mr-auto">
							<li class="nav-item active"><a class="nav-link"
								href="ItemMasterListServlet">himawari管理システム</a></li>
						</ul>
						<ul class="navbar-nav flex-row">
							<li class="nav-item"><a class="nav-link"
								href="ItemMasterListServlet">${loginManager.name}</a></li>
							<li class="nav-item"><a class="btn btn-primary"
								href="LogoutMasterServlet">ログアウト</a></li>
						</ul>
					</nav>
				</header>
				<div class="text-center">
					<h1>商品削除確認</h1>
					<h5>商品ID：${item.id}</h5>
					<h5>を本当に削除してよろしいでしょうか?</h5>
				</div>
				<form action="ItemMasterDeleteServlet" method="post">
					<input type="hidden" class="form-control" id="inputId" name="id"
						value="${item.id}">
					<div class="text-center">
						<div class="form-group row">
							<div class="col-sm-5">
								<a class="btn btn-primary center-block form-submit"
									href="ItemMasterListServlet">キャンセル</a>
							</div>
							<div class="col-sm-5">
								<button type="submit"
									class="btn btn-primary center-block form-submit">削除</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
