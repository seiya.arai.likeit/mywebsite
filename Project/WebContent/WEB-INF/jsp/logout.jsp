<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css"
	rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<!-- 共通独自CSS -->


</head>
<body>
	<nav class="yellow darken-3" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="HomeServlet" class="brand-logo">himawali</a>
			<ul class="right">

				<li><a href="UserDataServlet"><i class="material-icons">person_add</i></a></li>


				<li><a href="ShoppingCartServlet"><i class="material-icons">local_grocery_store</i></a></li>


				<li><a href="LoginServlet"><i class="material-icons">vpn_key</i></a></li>
			</ul>
		</div>
	</nav>

	<div class="container">
		<div class="row">
			<div class="section"></div>
			<div class="col s6 offset-s3">
				<div class="card grey lighten-5">
					<div class="card-content">
						<div class="row center">
							<h4>ログアウトしました!</h4>
						</div>
						<div class="row">
							<div class="col s12">
								<p class="center-align">
									<a href="HomeServlet"
										class="btn btn-large waves-effect waves-light  col s8 offset-s2">ホームへ</a>

								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>






	<footer class="page-footer yellow darken-3">
		<div class="container">
			<div class="right-align">Made by himawali</div>
		</div>
	</footer>