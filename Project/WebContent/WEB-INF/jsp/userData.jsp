<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員情報</title>
<!DOCTYPE html>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1.0" />
<!-- CSS  -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="Materialize/css/materialize.css" type="text/css"
	rel="stylesheet" media="screen,projection" />
<link href="Materialize/css/style.css" type="text/css" rel="stylesheet"
	media="screen,projection" />
<!-- 共通独自CSS -->

</head>
<body>

	<!DOCTYPE html>
	<nav class="yellow darken-3" role="navigation">
		<div class="nav-wrapper container">
			<a id="logo-container" href="HomeServlet" class="brand-logo">himawari</a>
			<ul class="right">



				<li><a href="HomeServlet"><i class="material-icons">home</i></a></li>


				<li><a href="ShoppingCartServlet"><i class="material-icons">local_grocery_store</i></a></li>


				<li><a href="LogoutServlet"><i class="material-icons">exit_to_app</i></a></li>

			</ul>
		</div>
	</nav>
	<br>
	<br>
	<div class="container">
		<div class="row center">
			<h4 class=" col s12 light">会員情報</h4>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<form action="UserDataUpdateServlet" method="POST">
							<c:if test="${message != null}">
								<p class="red-text center-align">${message}</p>
							</c:if>
							<br> <br>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" name="name" value="${lmb.name}">
									<label>名前</label>
								</div>
								<div class="input-field col s6">
									<input type="text" name="login_id" value="${lmb.loginId}">
									<label>ログインID</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s3">
									<input type="text" name="postalCode" value="${lmb.postalCode}">
									<label>郵便番号</label>
								</div>
								<div class="input-field col s9">
									<input type="text" name="address" value="${lmb.address}">
									<label>住所</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="text" name="phoneNumber" value="${lmb.phoneNumber}">
									<label>電話番号</label>
								</div>
								<div class="input-field col s6">
									<input type="date" name="birthDate" value="${lmb.birthDate}">
									<label>生年月日</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<input type="password" name="password1" value="${lmb.password}">
									<label>パスワード</label>
								</div>
								<div class="input-field col s6">
									<input type="password" name="password2" value="${lmb.password}">
									<label>パスワード（確認）</label>
								</div>
							</div>
							<div class="row">
								<div class="col s12">
									<button
										class="btn brown  waves-effect waves-light  col s4 offset-s4"
										type="submit" name="action">更新</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--  購入履歴 -->
		<div class="row">
			<div class="col s12">
				<div class="card grey lighten-5">
					<div class="card-content">
						<table class="bordered">
							<thead>
								<tr>
									<th style="width: 10%"></th>
									<th class="center">購入日時</th>
									<th class="center">配送方法</th>
									<th class="center">購入金額（合計）</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="bdb" items="${bdb}">
									<tr>
										<td class="center"><a
											href="UserBuyHistoryDetailServlet?buy_id=${bdb.id}"
											class="btn-floating btn waves-effect waves-light "> <i
												class="material-icons">details</i></a></td>
										<td class="center">${bdb.formatDate}</td>
										<td class="center">${bdb.deliveryMethodName}</td>
										<td class="center">${bdb.formatTotalPrice}円(税込)</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s8 offset-s2 ">
			<p class="right-align">
				<a href="UserDataServlet">トップへ</a>
			</p>
		</div>
	</div>
	<!DOCTYPE html>
	<footer class="page-footer yellow darken-3">
		<div class="container">
			<div class="right-align">Made by himawari</div>
		</div>
	</footer>
</body>
</html>