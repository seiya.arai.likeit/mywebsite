package beans;

import java.io.Serializable;
import java.util.Date;

public class ItemDataBeans implements Serializable {
	private int id;
	private String name;
	private String spec;
	private int price;
	private Date createDate;
	private Date updateDate;
	private String fileName;
	private int burandId;
	private String burandName;
	private String detail;

	//全ての商品情報を取得するためのコンストラクタ
	public ItemDataBeans(int id, String name, String spec, int price, Date createDate, Date updateDate, String fileName,
			int burandId, String burandName) {
		this.id = id;
		this.name = name;
		this.spec = spec;
		this.price = price;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.fileName = fileName;
		this.burandId = burandId;
		this.burandName = burandName;

	}

	//商品詳細のコンストラクタ
	public ItemDataBeans(int id, String name, String burandName, String detail, String spec, int price,
			Date createDate, Date updateDate) {
		this.id = id;
		this.name = name;
		this.burandName = burandName;
		this.detail = detail;
		this.spec = spec;
		this.price = price;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public ItemDataBeans() {

	}

	public ItemDataBeans(int id, String name, String burandName, String detail, String spec, int price,
			Date createDate, Date updateDate, String fileName) {
		this.id = id;
		this.name = name;
		this.burandName = burandName;
		this.detail = detail;
		this.spec = spec;
		this.price = price;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.fileName = fileName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpec() {
		return spec;
	}

	public void setSpec(String spec) {
		this.spec = spec;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getBurandId() {
		return burandId;
	}

	public void setBurandId(int burandId) {
		this.burandId = burandId;
	}

	public String getBurandName() {
		return burandName;
	}

	public void setBurandName(String burandName) {
		this.burandName = burandName;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}

	public String getIncludePrice() {
		return  String.format("%,d",(int)(this.price * 1.10));
	}

}
