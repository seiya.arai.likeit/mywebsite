package beans;

import java.io.Serializable;
import java.util.Date;

public class LoginManagerBeans implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String postalCode;
	private String address;
	private String phoneNumber;
	private Date createDate;
	private Date updateDate;

	//ログインセッションを保存するためのコンストラクタ
	public LoginManagerBeans(int id, String loginId, String name) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;

	}

	//全てのデータをセットするコンストラクタ
	public LoginManagerBeans(int id, String loginId, String name, Date birthDate, String password, String postalCode,
			String address, String phoneNumber, Date createDate, Date updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.postalCode = postalCode;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public LoginManagerBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	/**
	 * ユーザー情報更新の必要情報をまとめてセットするためのコンストラクタ
	 * @param name
	 * @param loginId
	 * @param postalCode
	 * @param address
	 * @param phoneNumber
	 * @param birthDate
	 * @param password
	 */
	public void setUpdateLoginManagerBeansInfo(String name, String loginId, String postalCode, String address,
			String phoneNumber, Date birthDate, String password1, String password2) {
		this.name = name;
		this.loginId = loginId;
		this.postalCode = postalCode;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.birthDate = birthDate;
		this.password = password1;
		this.password = password2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setUpdateLoginManagerBeansInfo(String parameter, String parameter2, String parameter3,
			String parameter4, String parameter5, String parameter6, String parameter7, String parameter8) {
		// TODO 自動生成されたメソッド・スタブ
		
	}

	



}
