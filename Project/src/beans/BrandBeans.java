package beans;

public class BrandBeans {
	private int id;
	private String brandName;

	public BrandBeans(int id, String name) {
		this.id = id;
		this.brandName = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

}
