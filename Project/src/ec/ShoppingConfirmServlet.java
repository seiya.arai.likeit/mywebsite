package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import dao.DeliveryMethodDAO;

/**
 * Servlet implementation class ShoppingConfirmServlet
 */
@WebServlet("/ShoppingConfirmServlet")
public class ShoppingConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ShoppingConfirmServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 商品購入画面
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin")
					: false;
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			if (!isLogin) {
				//sessionにリターンページ情報を書き込む
				session.setAttribute("returnStrUrl", "ShoppingConfirmServlet");
				//Login画面にリダイレクト
				response.sendRedirect("LoginServlet");
			} else if (cart.size() == 0) {
				request.setAttribute("cartActionMessage", "購入する商品がありません");
				request.getRequestDispatcher(EcHelper.SHOPPING_CART_PAGE).forward(request, response);
			} else {
				//配送方法をDBから取得
				ArrayList<DeliveryMethodDataBeans> dMDBList = DeliveryMethodDAO.getAllDeliveryMethodData();
				request.setAttribute("dmdbList", dMDBList);
				request.getRequestDispatcher(EcHelper.SHOPPING_CONFIRM_PAGE).forward(request, response);
			}

			//閲覧履歴を取得
			ArrayList<ItemDataBeans> browsing = (ArrayList<ItemDataBeans>) session.getAttribute("browsing");

			//セッション閲覧履歴がない場合閲覧履歴を作成
			if (browsing == null) {
				browsing = new ArrayList<ItemDataBeans>();
				session.setAttribute("detail", browsing);
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
		}
	}
}
