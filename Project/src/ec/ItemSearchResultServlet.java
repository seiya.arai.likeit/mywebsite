package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.EcItemDAO;

/**
 * Servlet implementation class ItemSearchResultServlet
 */
@WebServlet("/ItemSearchResultServlet")
public class ItemSearchResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//１ページに表示する商品数
	final static int PAGE_MAX_ITEM_COUNT = 8;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			String searchWord = request.getParameter("search_word");
			//表示ページ番号が未指定の場合１ページ目を表示　三項演算子を使用
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
			//検索されたキーワードをセッションに格納する
			session.setAttribute("searchWord", searchWord);

			//商品リストを取得ページ表示分のみ
			ArrayList<ItemDataBeans> searchResultItemList = EcItemDAO.getItemName(searchWord, pageNum,
					PAGE_MAX_ITEM_COUNT);

			//検索ワードに対しての総ページ数を取得
			double itemCount = EcItemDAO.getItemCount(searchWord);
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) itemCount);
			//総ページ数
			request.setAttribute("pageMax", pageMax);
			//表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("itemList", searchResultItemList);

			//閲覧履歴を取得
			ArrayList<ItemDataBeans> browsing = (ArrayList<ItemDataBeans>) session.getAttribute("browsing");

			//セッション閲覧履歴がない場合閲覧履歴を作成
			if (browsing == null) {
				browsing = new ArrayList<ItemDataBeans>();
				session.setAttribute("detail", browsing);
			}

			request.getRequestDispatcher(EcHelper.ITEM_SEARCH_RESULT_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
		}
	}
}
