package ec;


import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.LoginManagerBeans;
import dao.BuyDetailDAO;
import dao.EcUserDAO;

/**
 * Servlet implementation class UserDataServlet
 */
@WebServlet("/UserDataServlet")
public class UserDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDataServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 会員情報表示画面
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッション開始
		HttpSession session = request.getSession();
		try {
			//ログイン時に取得したユーザーIDをセッションから取得
			int userId = (int) session.getAttribute("userId");
			//更新確認画面から戻ってきた場合Sessionから取得。それ以外はuserIdでユーザーを取得
			LoginManagerBeans lmb = session.getAttribute("returnUDB") == null
					? EcUserDAO.getUserDataBeansByUserId(userId)
					: (LoginManagerBeans) EcHelper.cutSessionAttribute(session, "returnUDB");

			//入力された内容に誤りが会った時等に表示するエラーメッセージを格納する
			String message = (String) EcHelper.cutSessionAttribute(session, "message");


			BuyDetailDAO bdDao = new BuyDetailDAO();
			ArrayList<BuyDataBeans> bdb = bdDao.getBuyDataBeansListByBuyBuyId(userId);

			request.setAttribute("lmb", lmb);
			request.setAttribute("message", message);
			request.setAttribute("bdb", bdb);

			request.getRequestDispatcher(EcHelper.USER_DATA_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
		}
	}

}
