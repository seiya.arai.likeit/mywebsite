package ec;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.LoginManagerBeans;
import dao.EcUserDAO;

/**
 * Servlet implementation class UserCreateResultServlet
 */
@WebServlet("/UserCreateResultServlet")
public class UserCreateResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCreateResultServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		try {

			String name = request.getParameter("name");
			String birthDate = request.getParameter("birthDate");
			String postalCode = request.getParameter("postalCode");
			String address = request.getParameter("address");
			String phoneNumber = request.getParameter("phoneNumber");
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");

			LoginManagerBeans udb = new LoginManagerBeans();
			//String型のbirthDateをDate型に変更するための文
			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sdFormat.parse(birthDate);
			udb.setName(name);
			udb.setBirthDate(date);
			udb.setPostalCode(postalCode);
			udb.setAddress(address);
			udb.setPhoneNumber(phoneNumber);
			udb.setLoginId(loginId);
			udb.setPassword(password);

			// 登録が確定されたかどうか確認するための変数
			String confirmed = request.getParameter("confirm_button");

			switch(confirmed) {
			case "cancel":
				session.setAttribute("udb", udb);
				response.sendRedirect("UserCreateservlet");
				break;

			case "regist":
				EcUserDAO.createUser(udb);
				request.setAttribute("udb",udb);
				request.setAttribute("birthDate",birthDate);
				request.getRequestDispatcher(EcHelper.USER_CREATE_RESULT_PAGE).forward(request, response);
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
		}
	}

}
