package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.ItemDataBeans;
import dao.BuyDetailDAO;

/**
 * Servlet implementation class UserBuyHistoryDetailServlet
 */
@WebServlet("/UserBuyHistoryDetailServlet")
public class UserBuyHistoryDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserBuyHistoryDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 購入履歴画面
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {

			String buyId = request.getParameter("buy_id");

			//入力された内容に誤りがあった時に表示するエラーメッセージを格納する
			String message = (String) EcHelper.cutSessionAttribute(session, "message");

			BuyDetailDAO bdDao = new BuyDetailDAO();
			BuyDataBeans bdd2 = bdDao.getBuyDataBeansListByBuyBuyBuyId(buyId);
			ArrayList<ItemDataBeans> bdd3 = bdDao.getBuyDataBeansListByBuyBuyBuyBuyId(buyId);

			request.setAttribute("message", message);
			request.setAttribute("bdd2", bdd2);
			request.setAttribute("bdd3", bdd3);

			request.getRequestDispatcher(EcHelper.USER_BUY_HISTORY_DETAIL_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
		}
	}
}
