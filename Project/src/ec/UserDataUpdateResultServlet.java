package ec;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.LoginManagerBeans;
import dao.EcUserDAO;

/**
 * Servlet implementation class UserDataUpdateResultServlet
 */
@WebServlet("/UserDataUpdateResultServlet")
public class UserDataUpdateResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDataUpdateResultServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * ユーザー情報更新結果画面
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//セッション開始
		HttpSession session = request.getSession();
		try {
			//入力フォームから受け取った値をBeansにセット
			String name = request.getParameter("name");
			String loginId = request.getParameter("login_id");
			String postalCode = request.getParameter("postalCode");
			String address = request.getParameter("address");
			String phoneNumber = request.getParameter("phoneNumber");
			String birthDate = request.getParameter("birthDate");
			String password = request.getParameter("password1");

			LoginManagerBeans lmb = new LoginManagerBeans();

			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sdFormat.parse(birthDate);

			lmb.setName(name);
			lmb.setLoginId(loginId);
			lmb.setPostalCode(postalCode);
			lmb.setAddress(address);
			lmb.setPhoneNumber(phoneNumber);
			lmb.setPassword(password);

			//確定ボタンが押されたかを確認する変数
			String confirmed = request.getParameter("confirmButton");

			switch (confirmed) {
			//確定ボタンが押されていなかった場合はセッションに入力内容を保持してユーザー情報画面へ
			case "cancel":
				session.setAttribute("returnUDB", lmb);
				session.setAttribute("returnUDB", birthDate);
				response.sendRedirect("UserDataServlet");
				break;

			//アップデート処理
			case "update":
				EcUserDAO.updateUser(lmb);
				request.setAttribute("lmb", lmb);
				request.setAttribute("birthDate", birthDate);
				request.getRequestDispatcher(EcHelper.USER_DATA_UPDATE_RESULT_PAGE).forward(request, response);
				break;

			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
