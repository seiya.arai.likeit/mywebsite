package ec;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.EcItemDAO;

/**
 * Servlet implementation class ItemDetailServlet
 */
@WebServlet("/ItemDetailServlet")
public class ItemDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//選択された商品のIDを型変換し利用　String型をIntに変換
			int id = Integer.parseInt(request.getParameter("item_id"));
			//戻るページ表示用　String型をIntに変換
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));
			//対象のアイテム情報を取得
			ItemDataBeans item = EcItemDAO.getItemId(id);
			//リクエストパラメーターにセット
			request.setAttribute("item", item);
			request.setAttribute("pageNum", pageNum);

			//閲覧履歴を取得
			ArrayList<ItemDataBeans> browsing = (ArrayList<ItemDataBeans>) session.getAttribute("browsing");

			//セッション閲覧履歴がない場合閲覧履歴を作成
			if (browsing == null) {
				browsing = new ArrayList<ItemDataBeans>();
			}
			//閲覧履歴に商品を追加
				browsing.add(item);

			//閲覧履歴の情報更新
			session.setAttribute("browsing", browsing);
			request.getRequestDispatcher(EcHelper.ITEM_DETAIL_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
		}
	}
}
