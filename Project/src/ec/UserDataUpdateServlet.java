package ec;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.LoginManagerBeans;
import dao.EcUserDAO;

/**
 * Servlet implementation class UserDataUpdateServlet
 */
@WebServlet("/UserDataUpdateServlet")
public class UserDataUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDataUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * ユーザー情報更新確認画面
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//セッション開始
		HttpSession session = request.getSession();
		try {
			//エラーメッセージを格納する変数
			String message = "";

			//入力フォームから受け取った値をBeansにセット
			String name = request.getParameter("name");
			String loginId = request.getParameter("login_id");
			String postalCode = request.getParameter("postalCode");
			String address = request.getParameter("address");
			String phoneNumber = request.getParameter("phoneNumber");
			String birthDate = request.getParameter("birthDate");
			String password1 = request.getParameter("password1");
			String password2 = request.getParameter("password2");

			LoginManagerBeans lmb = new LoginManagerBeans();

			//String型のbirthDateをDate型に変更するための文
			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = sdFormat.parse(birthDate);

			lmb.setName(name);
			lmb.setLoginId(loginId);
			lmb.setPostalCode(postalCode);
			lmb.setAddress(address);
			lmb.setPhoneNumber(phoneNumber);
			lmb.setPassword(password1);
			

			//入力されているパスワードが確認と等しいか
			if (!password1.equals(password2)) {
				message = "入力されているパスワードと確認用パスワードが違います";
			}
			//ログインIDの入力規則チェック 英数字 ハイフン アンダースコアのみ入力可能
			if (!EcHelper.isLoginIdValidation(lmb.getLoginId())) {
				message = "半角英数字とハイフン、アンダースコアのみ入力できます";
			}
			//loginIdの重複をチェック
			if (EcUserDAO.isOverlapLoginId(lmb.getLoginId(), (int) session.getAttribute("userId"))) {
				message = "他のユーザーが使用中のログインIDです。";
			}

			//エラーメッセージがないなら確認画面へ
			if (message.length() == 0) {
				//確認画面へ
				request.setAttribute("lmb", lmb);
				request.setAttribute("birthDate", birthDate);
				request.getRequestDispatcher(EcHelper.USER_DATA_UPDATE_PAGE).forward(request, response);
			} else {
				//セッションにエラーメッセージを持たせてユーザー画面へ
				session.setAttribute("message", message);
				response.sendRedirect("UserDataServlet");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
		}
	}

}
