package ec;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

public class EcHelper {
	//ログイン画面
	static final String LOGIN_PAGE ="WEB-INF/jsp/login.jsp";
	//ログアウト画面
	static final String LOGOUT_PAGE ="WEB-INF/jsp/logout.jsp";
	//新規登録画面
	static final String USER_CREATE_PAGE ="WEB-INF/jsp/userCreate.jsp";
	//新規登録入力内容確認画面
	static final String USER_CREATE_CONFIRM_PAGE ="WEB-INF/jsp/userCreateConfirm.jsp";
	//新規登録完了画面
	static final String USER_CREATE_RESULT_PAGE ="WEB-INF/jsp/userCreateResult.jsp";
	//ホーム画面
	static final String HOME_PAGE ="WEB-INF/jsp/home.jsp";
	//商品詳細画面
	static final String ITEM_DETAIL_PAGE ="WEB-INF/jsp/itemDetail.jsp";
	//検索結果画面
	static final String ITEM_SEARCH_RESULT_PAGE ="WEB-INF/jsp/itemSearchResult.jsp";
	//買い物かご確認画面
	static final String SHOPPING_CART_PAGE ="WEB-INF/jsp/shoppingCart.jsp";
	//購入最終確認画面
	static final String SHOPPING_BUY_PAGE ="WEB-INF/jsp/shoppingBuy.jsp";
	//購入画面
	static final String SHOPPING_CONFIRM_PAGE ="WEB-INF/jsp/shoppingConfirm.jsp";
	//購入完了画面
	static final String SHOPPING_RESULT_PAGE ="WEB-INF/jsp/shoppingResult.jsp";
	//会員情報画面
	static final String USER_DATA_PAGE ="WEB-INF/jsp/userData.jsp";
	//会員情報更新画面
	static final String USER_DATA_UPDATE_PAGE ="WEB-INF/jsp/userDataUpdate.jsp";
	//会員情報更新完了画面
	static final String USER_DATA_UPDATE_RESULT_PAGE ="WEB-INF/jsp/userDataUpdateResult.jsp";
	//購入履歴詳細画面
	static final String USER_BUY_HISTORY_DETAIL_PAGE ="WEB-INF/jsp/userBuyHistoryDetail.jsp";

	public static EcHelper getInstance() {
		return new EcHelper();
	}

	/**
	 * セッションから指定データを取得（削除も一緒に行う）
	 */
	public static Object cutSessionAttribute(HttpSession session, String str) {
		Object test = session.getAttribute(str);
		session.removeAttribute(str);

		return test;
	}

	/**
	 * ハッシュ関数
	 * @param target
	 * @return
	 */
	public static String getSha256(String target) {
		MessageDigest md = null;
		StringBuffer buf = new StringBuffer();
		try {
			md = MessageDigest.getInstance("SHA-256");
			md.update(target.getBytes());
			byte[] digest = md.digest();

			for (int i = 0; i < digest.length; i++) {
				buf.append(String.format("%02x", digest[i]));
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return buf.toString();
	}

	/**
	 * ログインIDの作成制限
	 *
	 * @param inputLoginId
	 * @return
	 */
	public static boolean isLoginIdValidation(String inputLoginId) {
		// 英数字アンダースコア以外が入力されていたら
		if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}

		return false;

	}
	/**
	 * 商品の合計金額を算出する
	 * @param items
	 * @return total
	 */
	public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
		int total = 0;
		for(ItemDataBeans item : items) {
			total += item.getPrice();
		}
		return total;
	}

}
