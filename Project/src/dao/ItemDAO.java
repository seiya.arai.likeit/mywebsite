package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDAO {
	public List<ItemDataBeans> findAll() {
		Connection conn = null;
		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM item  JOIN burand ON item.burand_id = burand.id";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String spec = rs.getString("spec");
				int price = rs.getInt("price");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_date");
				String fileName = rs.getString("file_name");
				int burandId = rs.getInt("burand_id");
				String burandName = rs.getString("burand_name");

				ItemDataBeans item = new ItemDataBeans(id, name, spec, price, createDate, updateDate, fileName,
						burandId, burandName);
				itemList.add(item);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

	//キャンプギア新規登録画面
	public void itemCreate(String name, String spec, String price, String fileName, String burandId, String detail) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "INSERT INTO item(name ,spec , price , create_date , update_date , file_name , burand_id , detail) VALUES (? , ? , ? , NOW() , NOW() , ? , ? , ?)";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, spec);
			pStmt.setString(3, price);
			pStmt.setString(4, fileName);
			pStmt.setString(5, burandId);
			pStmt.setString(6, detail);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	//キャンプギア詳細画面
	public ItemDataBeans itemDetail(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item JOIN burand ON item.burand_id = burand.id WHERE item.id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			//ログイン失敗時の処理
			if (!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String name = rs.getString("name");
			String burandName = rs.getString("burand_name");
			String detail = rs.getString("detail");
			String spec = rs.getString("spec");
			int price = rs.getInt("price");
			Date createDate = rs.getDate("create_date");
			Date updateDate = rs.getDate("update_date");
			String fileName = rs.getString("file_name");
			return new ItemDataBeans(idData, name, burandName, detail, spec, price, createDate, updateDate, fileName);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void itemUpdate(String name, String burandName, String price, String detail, String spec, String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "UPDATE item JOIN burand ON item.burand_id = burand.id SET item.name=?,burand.burand_name=?,item.price=?,item.detail=?,item.spec=?,item.update_date=now() WHERE item.id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, burandName);
			pStmt.setString(3, price);
			pStmt.setString(4, detail);
			pStmt.setString(5, spec);
			pStmt.setString(6, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void itemDelete(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "DELETE FROM item WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		}
	}

	public List<ItemDataBeans> findSearch(String itemIdP, String nameP, String burandP, String startDate,
			String endDate) {
		Connection conn = null;
		List<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM item  JOIN burand ON item.burand_id = burand.id WHERE item.id > 0";

			if (!itemIdP.equals("")) {
				sql += " AND item.id = '" + itemIdP + "'";
			}
			if (!nameP.equals("")) {
				sql += " AND item.name LIKE '%" + nameP + "%'";
			}
			if (!burandP.equals("")) {
				sql += " AND burand.burand_name LIKE '%" + burandP + "%'";
			}
			if (!startDate.equals("")) {
				sql += " AND item.create_date >= '" + startDate + "'";
			}
			if (!endDate.equals("")) {
				sql += " AND item.create_date <= '" + endDate + "'";
			}
			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String spec = rs.getString("spec");
				int price = rs.getInt("price");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_date");
				String fileName = rs.getString("file_name");
				int burandId = rs.getInt("burand_id");
				String burandName = rs.getString("burand_name");

				ItemDataBeans item = new ItemDataBeans(id, name, spec, price, createDate, updateDate, fileName,
						burandId, burandName);
				itemList.add(item);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}

}