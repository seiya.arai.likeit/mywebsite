package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BrandBeans;

public class BrandDao {
	public List<BrandBeans> findAll() {
		Connection conn = null;
		List<BrandBeans> itemList = new ArrayList<BrandBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM burand";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("burand_name");

				BrandBeans item = new BrandBeans(id, name);
				itemList.add(item);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return itemList;
	}
}
