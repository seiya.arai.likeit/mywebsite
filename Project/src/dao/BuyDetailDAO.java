package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyDataBeans;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

public class BuyDetailDAO {

	/**
	 * 購入詳細登録処理
	 * @param bddb　BuyDetailDataBeans
	 * @throws SQLException
	 */
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO buy_detail(buy_id,item_id) VALUES(?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();

			System.out.println("inserting BuyDetail has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入情報検索
	 * @param buyId
	 * @return　buyDetailList
	 * @throws SQLException
	 */
	public ArrayList<BuyDetailDataBeans> getBuyDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM buy_detail WHERE buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDetailDataBeans> buyDetailList = new ArrayList<BuyDetailDataBeans>();

			while (rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();
				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("buy_id"));
				bddb.setItemId(rs.getInt("item_id"));
				buyDetailList.add(bddb);
			}
			System.out.println("searching BuyDataBeansList by BuyID has been completed");

			return buyDetailList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 購入IDによる購入詳細情報検索
	 * @param buyId
	 * @return　buyDetailItemList　購入詳細情報のデータを持つ JavaBeansのリスト
	 *
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT item.id,"
							+ " item.name,"
							+ " item.price"
							+ " FROM buy_detail"
							+ " JOIN item"
							+ " ON buy_detail.item_id = item.id"
							+ " WHERE buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> buyDetailItemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));

				buyDetailItemList.add(idb);

			}

			System.out.println("searching ItemDataBeansList by BuyID has been completed");

			return buyDetailItemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * ログイン中のユーザーの購入履歴を表示する
	 * @param buyId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<BuyDataBeans> getBuyDataBeansListByBuyBuyId(int buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM buy JOIN delivery_method ON buy.delivery_method_id = delivery_method.id WHERE user_id = ? ORDER BY create_date DESC");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDataBeans> buyDataList = new ArrayList<BuyDataBeans>();

			while (rs.next()) {
				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setId(rs.getInt("buy.id"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setDeliveryMethodName(rs.getString("delivery_method.name"));
				bdb.setBuyDate(rs.getTimestamp("create_date"));
				bdb.setDeliveryMethodPrice(rs.getInt("delivery_method.price"));
				buyDataList.add(bdb);

			}
			System.out.println("searching BuyDataBeansList by BuyID has been completed");
			return buyDataList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 *	配送方法と購入情報の参照画面
	 * @param buyId
	 * @return
	 * @throws SQLException
	 */
	public BuyDataBeans getBuyDataBeansListByBuyBuyBuyId(String buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM buy JOIN delivery_method ON buy.delivery_method_id = delivery_method.id WHERE buy.id = ?");
			st.setString(1, buyId);

			ResultSet rs = st.executeQuery();
			BuyDataBeans bddb = new BuyDataBeans();
			if (rs.next()) {
				bddb.setId(rs.getInt("id"));
				bddb.setUserId(rs.getInt("user_id"));
				bddb.setTotalPrice(rs.getInt("total_price"));
				bddb.setDeliveryMethodName(rs.getString("name"));
				bddb.setBuyDate(rs.getTimestamp("create_date"));
				bddb.setDeliveryMethodPrice(rs.getInt("price"));
			}
			System.out.println("searching BuyDataBeansList by BuyID has been completed");
			return bddb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品の購入詳細
	 */
	public ArrayList<ItemDataBeans> getBuyDataBeansListByBuyBuyBuyBuyId(String buyId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM buy_detail JOIN item ON buy_detail.item_id = item.id JOIN burand ON burand.id = item.burand_id WHERE buy_id =?");
			st.setString(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> buyDataList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans bddb = new ItemDataBeans();
				bddb.setId(rs.getInt("buy_id"));
				bddb.setName(rs.getString("name"));
				bddb.setPrice(rs.getInt("price"));
				bddb.setBurandName(rs.getString("burand_name"));
				buyDataList.add(bddb);

			}

			System.out.println("searching BuyDataBeansList by BuyID has been completed");
			return buyDataList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
