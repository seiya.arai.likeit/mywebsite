package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.LoginManagerBeans;

public class UserDAO {

	public LoginManagerBeans loginMnager(String loginId, String password) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and login_password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new LoginManagerBeans(id, loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<LoginManagerBeans> findAll() {
		Connection conn = null;
		List<LoginManagerBeans> userList = new ArrayList<LoginManagerBeans>();
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("login_password");
				String postalCode = rs.getString("postalCode");
				String address = rs.getString("address");
				String phoneNumber = rs.getString("phoneNumber");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_Date");
				LoginManagerBeans user = new LoginManagerBeans(id, loginId, name, birthDate, password, postalCode,
						address, phoneNumber, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return userList;
	}

	public List<LoginManagerBeans> findSearch(String loginIdP, String nameP, String startDate, String endDate) {
		Connection conn = null;
		List<LoginManagerBeans> userList = new ArrayList<LoginManagerBeans>();

		try {
			//データベースへ接続
			conn = DBManager.getConnection();
			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id > 0";

			if (!loginIdP.equals("")) {
				sql += " AND login_id LIKE '%" + loginIdP + "%'";
			}

			if (!nameP.equals("")) {
				sql += " AND name LIKE '%" + nameP + "%'";
			}

			if (!startDate.equals("")) {
				sql += " AND birth_date >= '" + startDate + "'";
			}

			if (!endDate.equals("")) {
				sql += " AND birth_date <= '" + endDate + "'";
			}

			// SELECTを実行し、結果表を取得;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("login_password");
				String postalCode = rs.getString("postalCode");
				String address = rs.getString("address");
				String phoneNumber = rs.getString("phoneNumber");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_Date");
				LoginManagerBeans user = new LoginManagerBeans(id, loginId, name, birthDate, password, postalCode,
						address, phoneNumber, createDate, updateDate);
				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return userList;
	}

	public LoginManagerBeans userMasterDetail(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			int idData = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("login_password");
			String postalCode = rs.getString("postalcode");
			String address = rs.getString("address");
			String phoneNumber = rs.getString("phoneNumber");
			Date createDate = rs.getDate("create_date");
			Date updateDate = rs.getDate("update_date");
			return new LoginManagerBeans(idData, loginId, name, birthDate, password, postalCode, address, phoneNumber,
					createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			//データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public void userMasterUpdate(String loginId, String password, String name, String birthDate, String postalCode,
			String address, String phoneNumber, String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET login_id=?, login_password=?, name=?, birth_date=?, postalCode=?, address=?, phoneNumber=? ,update_date=now() WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);
			pStmt.setString(5, postalCode);
			pStmt.setString(6, address);
			pStmt.setString(7, phoneNumber);
			pStmt.setString(8, id);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void userMasterDelete(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void userPass(String loginId, String name, String birthDate, String postalCode, String address,
			String phoneNumber, String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "UPDATE user SET login_id=?, name=?, birth_date=?, postalCode=?, address=?, phoneNumber=? WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, postalCode);
			pStmt.setString(5, address);
			pStmt.setString(6, phoneNumber);
			pStmt.setString(7, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
