package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import base.DBManager;
import beans.LoginManagerBeans;

public class EcUserDAO {

	/**
	 * ログイン：ユーザーIDを取得
	 * @param loginId:ログインID
	 * @param password:パスワード
	 * @return ログインIDとパスワードが正しい場合対象のユーザーID 正しくない登録されていない場合0
	 * @throws SQLException
	 */
	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
				//後で暗号化する
				if (password.equals(rs.getString("login_password"))) {
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}
			System.out.println("searching userId by loginId has been completed");
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static void createUser(LoginManagerBeans udb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO user(name,postalCode,address,phoneNumber,login_id,login_password,create_date,update_date,birth_date)VALUES(?,?,?,?,?,?,NOW(),NOW(),?)");
			st.setString(1, udb.getName());
			st.setString(2, udb.getPostalCode());
			st.setString(3, udb.getAddress());
			st.setString(4, udb.getPhoneNumber());
			st.setString(5, udb.getLoginId());
			//後で暗号化する
			st.setString(6, udb.getPassword());
			String str = new SimpleDateFormat("yyyy-MM-dd").format(udb.getBirthDate());

			st.setString(7, str);
			st.executeUpdate();
			System.out.println("inserting user has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * loginIdの重複チェック
	 *
	 * @param loginId
	 *            check対象のログインID
	 * @param userId
	 *            check対象から除外するuserID
	 * @return bool 重複している
	 * @throws SQLException
	 */
	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	/**
	 * ログインしているユーザー情報を取得
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public static LoginManagerBeans getUserDataBeansByUserId(int userId) throws SQLException {
		LoginManagerBeans ldb = new LoginManagerBeans();
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ldb.setId(rs.getInt("id"));
				ldb.setName(rs.getString("name"));
				ldb.setPostalCode(rs.getString("postalCode"));
				ldb.setAddress(rs.getString("address"));
				ldb.setPhoneNumber(rs.getString("phoneNumber"));
				ldb.setLoginId(rs.getString("login_id"));
				ldb.setPassword(rs.getString("login_password"));
				ldb.setBirthDate(rs.getDate("birth_date"));
			}
			st.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("searching UserDataBeans by userId has been completed");
		return ldb;
	}

	/**
	 * ユーザー情報の更新処理を行う
	 * @param lmb
	 * @throws SQLException
	 */
	public static void updateUser(LoginManagerBeans lmb) throws SQLException {
		//更新された情報をセットされたJavaBeansのリスト
		LoginManagerBeans updateLmb = new LoginManagerBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {

			con = DBManager.getConnection();
			st = con.prepareStatement(
					"UPDATE user SET name = ?, login_id = ?, postalCode = ?, address= ?, phoneNumber= ?, birth_date= ?, login_password = ?,  update_date = now() WHERE id = ?;");
			st.setString(1, lmb.getName());
			st.setString(2, lmb.getLoginId());
			st.setString(3, lmb.getPostalCode());
			st.setString(4, lmb.getAddress());
			st.setString(5, lmb.getPhoneNumber());
			st.setDate(6, (Date) lmb.getBirthDate());
			st.setString(7, lmb.getPassword());
			st.setInt(8, lmb.getId());
			st.executeUpdate();
			System.out.println("update has been completed");

			st = con.prepareStatement(
					"SELECT name, login_id,postalCode,address,phoneNumber,birth_date,login_password FROM user WHERE id ="
							+ lmb.getId());
			ResultSet rs = st.executeQuery();

			while (rs.next()) {

				updateLmb.setName(rs.getString("name"));
				updateLmb.setLoginId(rs.getString("login_id"));
				updateLmb.setPostalCode(rs.getString("postalCode"));
				updateLmb.setAddress(rs.getString("address"));
				updateLmb.setPhoneNumber(rs.getString("phoneNumber"));
				updateLmb.setBirthDate(rs.getDate("birth_date"));
				updateLmb.setPassword(rs.getString("login_password"));
			}
			st.close();
			System.out.println("searching updated-LoginManagerBeans has been completed");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
