package manager;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.LoginManagerBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemMasterListServlet
 */
@WebServlet("/ItemMasterListServlet")
public class ItemMasterListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemMasterListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		LoginManagerBeans user = (LoginManagerBeans) session.getAttribute("loginManager");
		if (user == null) {
			response.sendRedirect("LoginManagerServlet");
			return;
		}

		ItemDAO itemDao = new ItemDAO();
		List<ItemDataBeans> itemList = itemDao.findAll();
		request.setAttribute("itemMasterList", itemList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String itemId = request.getParameter("id");
		String name = request.getParameter("name");
		String burand = request.getParameter("burandName");
		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");

		ItemDAO itemdao = new ItemDAO();
		List<ItemDataBeans> list = itemdao.findSearch(itemId, name, burand, startDate, endDate);
		request.setAttribute("itemMasterList", list);
		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterList.jsp");
		dispatcher.forward(request, response);
	}

}
