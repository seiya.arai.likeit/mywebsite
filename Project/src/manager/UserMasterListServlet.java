package manager;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.LoginManagerBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserMasterListServlet
 */
@WebServlet("/UserMasterListServlet")
public class UserMasterListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserMasterListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		LoginManagerBeans user = (LoginManagerBeans) session.getAttribute("loginManager");
		if (user == null) {
			response.sendRedirect("LoginManagerServlet");
			return;
		}
		UserDAO userDao = new UserDAO();
		List<LoginManagerBeans> userList = userDao.findAll();
		request.setAttribute("userMasterList", userList);

		// ユーザ一覧のjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userMasterList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String startDate = request.getParameter("birthDateStart");
		String endDate = request.getParameter("birthDateEnd");

		UserDAO userdao = new UserDAO();
		List<LoginManagerBeans> user = userdao.findSearch(loginId, name, startDate, endDate);
		request.setAttribute("userMasterList", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userMasterList.jsp");
		dispatcher.forward(request, response);
	}

}
