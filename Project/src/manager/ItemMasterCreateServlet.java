package manager;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.BrandBeans;
import beans.LoginManagerBeans;
import dao.BrandDao;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemMasterCreateServlet
 */
@WebServlet("/ItemMasterCreateServlet")
@MultipartConfig(location="/Users/araiseiya/Documents/MyWebSite/Project/WebContent/img", maxFileSize=5048576)
public class ItemMasterCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		LoginManagerBeans user = (LoginManagerBeans) session.getAttribute("loginManager");
		if (user == null) {
			response.sendRedirect("LoginManagerServlet");
			return;
		}
		BrandDao branDao = new BrandDao();
		List<BrandBeans> bList = branDao.findAll();

		request.setAttribute("bList", bList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String burand = request.getParameter("burand");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String spec = request.getParameter("spec");
		Part part = request.getPart("fileName");
		String fileName = this.getFileName(part);
		part.write(fileName);
		ItemDAO itemDao = new ItemDAO();
		itemDao.itemCreate(name,spec,price,fileName,burand,detail);
		response.sendRedirect("ItemMasterListServlet");

	}

	 private String getFileName(Part part) {
	        String name = null;
	        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
	            if (dispotion.trim().startsWith("filename")) {
	                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
	                name = name.substring(name.lastIndexOf("\\") + 1);
	                break;
	            }
	        }
	        return name;
	    }

}
