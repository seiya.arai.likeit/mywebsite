package manager;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.LoginManagerBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserMasterDetailServlet
 */
@WebServlet("/UserMasterDetailServlet")
public class UserMasterDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserMasterDetailServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		LoginManagerBeans user = (LoginManagerBeans) session.getAttribute("loginManager");
		if (user == null) {
			response.sendRedirect("LoginManagerServlet");
			return;
		}

		String id = request.getParameter("id");

		UserDAO userDao = new UserDAO();
		LoginManagerBeans users = userDao.userMasterDetail(id);

		request.setAttribute("userMasterDetail", users);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userMasterDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	}

}
