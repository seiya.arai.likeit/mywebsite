package manager;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.LoginManagerBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserMasterUpdateServlet
 */
@WebServlet("/UserMasterUpdateServlet")
public class UserMasterUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserMasterUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		LoginManagerBeans user = (LoginManagerBeans) session.getAttribute("loginManager");
		if (user == null) {
			response.sendRedirect("LoginManagerServlet");
			return;
		}

		String id = request.getParameter("id");
		UserDAO userDao = new UserDAO();
		LoginManagerBeans users = userDao.userMasterDetail(id);
		request.setAttribute("userMasterDetail", users);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userMasterUpdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String postalCode = request.getParameter("postalCode");
		String address = request.getParameter("address");
		String phoneNumber = request.getParameter("phoneNumber");
		String id = request.getParameter("id");

		UserDAO userDao = new UserDAO();

		if (password.equals("") && password2.equals("")) {
			userDao.userPass(loginId, name, birthDate, postalCode, address, phoneNumber, id);
		} else if (!password.equals(password2)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userMasterUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		} else if (loginId.equals("") || name.equals("") || birthDate.equals("") || postalCode.equals("")
				|| address.equals("") || phoneNumber.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		} else {
			userDao.userMasterUpdate(loginId, password, name, birthDate, postalCode, address, phoneNumber, id);
		}
		response.sendRedirect("UserMasterListServlet");

	}
}
