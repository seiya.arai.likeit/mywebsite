package manager;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.LoginManagerBeans;
import dao.ItemDAO;

/**
 * Servlet implementation class ItemMasterUpdateServlet
 */
@WebServlet("/ItemMasterUpdateServlet")
public class ItemMasterUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemMasterUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		LoginManagerBeans user = (LoginManagerBeans) session.getAttribute("loginManager");
		if (user == null) {
			response.sendRedirect("LoginManagerServlet");
			return;
		}

		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");

		ItemDAO itemDao = new ItemDAO();
		ItemDataBeans item = itemDao.itemDetail(id);

		request.setAttribute("item", item);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/itemMasterUpdate.jsp");
		dispatcher.forward(request, response);
}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String burandName = request.getParameter("burandName");
		String price = request.getParameter("price");
		String detail = request.getParameter("detail");
		String spec = request.getParameter("spec");
		String id = request.getParameter("id");

		ItemDAO itemDao = new ItemDAO();
		itemDao.itemUpdate(name,burandName,price,detail,spec,id);
		response.sendRedirect("ItemMasterListServlet");


	}

}
